package icd0004.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import icd0004.model.Report;
import icd0004.myExceptions.FileFormatException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FileUtil {
    public static List<String> readInput(String inputPath) throws IOException, FileFormatException {
        List<String> result = new LinkedList<>();

        if (!isTxtFile(inputPath)) {
            String fileExt = inputPath.substring(inputPath.lastIndexOf(".") + 1);
            throw new FileFormatException("Illegal file extension: '" + fileExt + "'");
        }

        File file = new File(inputPath);

        Scanner sc = new Scanner(file, StandardCharsets.UTF_8);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (!line.trim().equals("")) {
                result.add(line.trim());
            }
        }
        return result;
    }

    private static boolean isTxtFile(String inputPath) {
        String fileExt = inputPath.substring(inputPath.lastIndexOf(".") + 1);
        return fileExt.equals("txt");
    }

    public static void writeReportToFile(Report report, String dir) throws FileAlreadyExistsException, IOException {
        String fullPath = dir + "\\" + generateNameForReport(report);

        ObjectMapper mapper = new ObjectMapper();
        File output = new File(fullPath);

        if (output.createNewFile()) {
            mapper.writeValue(output, report);
        } else {
            throw new FileAlreadyExistsException("File already exists: " + output.getAbsolutePath());

        }
    }

    private static String generateNameForReport(Report report) {

        return report.getReportDetails().getCity() + "_" + report.getCurrentWeatherReport().getDate() + ".json";
    }
}
