package icd0004.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import icd0004.model.Report;
import icd0004.model.WeatherReport;

import java.io.IOException;
import java.util.List;

public class JSONParser {

    private static final ObjectMapper mapper = new ObjectMapper();

    public static String writeAsString(Report report) throws JsonProcessingException {
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(report);
    }

    public WeatherReport parseCurrentWeather(String json) throws JsonProcessingException {
        WeatherReport report = new WeatherReport();

        JsonNode parentNode = mapper.readTree(json);
        JsonNode main = parentNode.get("main");

        report.setTemperature(main.get("temp").asDouble());
        report.setPressure(main.get("pressure").asInt());
        report.setHumidity(main.get("humidity").asInt());
        report.setDate(parentNode.get("dt").asLong());
        report.setTemperatureUnit("Celsius");

        if (parentNode.has("city")) {
            parentNode = parentNode.get("city");
        }
        JsonNode coord = parentNode.get("coord");
        report.setName(parentNode.get("name").asText());
        report.setLon(coord.get("lon").asDouble());
        report.setLat(coord.get("lat").asDouble());

        return report;
    }

    public List<WeatherReport> parseWeatherForecast(String json) throws IOException {
        JsonNode parentNode = mapper.readTree(json);
        JsonNode cityNode = parentNode.get("city");
        JsonNode weatherList = parentNode.get("list");
        ObjectReader reader = mapper.readerFor(new TypeReference<List<WeatherReport>>() {
        });
        List<WeatherReport> list = reader.readValue(weatherList);

        String name = cityNode.get("name").asText();

        Double lon = (cityNode.get("coord").get("lon").asDouble());
        lon = Math.round(lon * 100.0) / 100.0;//Round to two decimal
        Double lat = cityNode.get("coord").get("lat").asDouble();
        lat = Math.round(lat * 100.0) / 100.0;

        for (WeatherReport report : list) {
            report.setTemperatureUnit("Celsius");
            report.setName(name);
            report.setLon(lon);
            report.setLat(lat);
        }

        return list;
    }
}
