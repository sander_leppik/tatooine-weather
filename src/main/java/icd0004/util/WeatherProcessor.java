package icd0004.util;

import icd0004.model.WeatherReport;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class WeatherProcessor {

    public static List<WeatherReport> consolidateWeatherForecast(List<WeatherReport> detailedReports) {
        Map<ZonedDateTime, List<WeatherReport>> map = mapReports(detailedReports);
        List<WeatherReport> resultList = new LinkedList<>();

        for (List<WeatherReport> reports : map.values()) {
            resultList.add(calculateAverage(reports));
        }

        return resultList;
    }

    private static WeatherReport calculateAverage(List<WeatherReport> detailedReports) {
        WeatherReport report = detailedReports.get(0);

        long dt = report.getDate();
        String tempUnit = report.getTemperatureUnit();
        String city = report.getName();
        Double lon = report.getLon();
        Double lat = report.getLat();

        double temperature = 0;
        double humidity = 0;
        double pressure = 0;
        int counter = 0;
        for (WeatherReport detailedReport : detailedReports) {
            temperature += detailedReport.getTemperature();
            humidity += detailedReport.getHumidity();
            pressure += detailedReport.getPressure();
            counter++;
        }

        temperature = temperature / counter;
        Integer avgHumidity = Math.toIntExact(Math.round(humidity / counter));
        Integer avgPressure = Math.toIntExact(Math.round(pressure / counter));

        return new WeatherReport(dt, temperature, avgHumidity, avgPressure, tempUnit, city, lon, lat);
    }

    private static Map<ZonedDateTime, List<WeatherReport>> mapReports(List<WeatherReport> reports) {
        Map<ZonedDateTime, List<WeatherReport>> map = new LinkedHashMap<>();
        ZonedDateTime date;

        List<WeatherReport> tempList;
        for (WeatherReport report : reports) {
            date = getStartOfDay(report.getDate());
            report.setDate(date.toEpochSecond());

            if (map.containsKey(date)) {
                tempList = map.get(date);
            } else {
                tempList = new LinkedList<>();
                map.put(date, tempList);
            }
            tempList.add(report);
        }

        return map;
    }

    private static ZonedDateTime getStartOfDay(long dt) {
        ZonedDateTime zdt = ZonedDateTime.ofInstant(Instant.ofEpochSecond(dt), ZoneId.of("UTC"));

        return ZonedDateTime.of(zdt.getYear(), zdt.getMonthValue(), zdt.getDayOfMonth(),
                0, 0, 0, 0, ZoneId.of("UTC"));
    }
}
