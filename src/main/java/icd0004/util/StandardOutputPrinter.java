package icd0004.util;

import icd0004.myExceptions.FileFormatException;

import static java.lang.System.out;

public class StandardOutputPrinter {

    private String stdout;

    public String getLastPrint() {
        return stdout;
    }

    public void stdoutInputNotFound(String input) {
        stdout = "Input file not found: " + input;
        out.println(stdout);
    }

    public void stdoutIllegalFileFormat(FileFormatException e) {
        out.println(e.getMessage());
        out.println("Only '.txt' files allowed for input.");
    }

    public void stdoutCityNotFound(String city) {
        stdout = "City not found: " + city;
        out.println(stdout);
    }

    public void stdout(String output) {
        stdout = output;
        out.println(output);
    }

    public void stdoutStandardOrFile() {
        out.println("Arguments must have either '-city' or '-F' and '-O'. Not both.");
    }

    public void stdoutArgumentsMissing() {
        out.println("Arguments missing. Type '-H' for help.");
    }

    public void stdoutNoSuchArgument(String arg) {
        out.println("No such argument: " + arg);
    }

    public void stdoutHelp() {
        out.println("Possible arguments are:");
        out.println("'-H' - Help");
        out.println("[SELECT] '-SIMPLE' or '-FULL'");
        out.println("         '-SIMPLE' - for current weather report");
        out.println("         '-FULL' - for current weather and weather forecast report");
        out.println("[SELECT] '-city' or '-F' and '-O'");
        out.println("         '-city' - name of the city for the weather report. Standard output.");
        out.println("         '-F' - Absolute path for the input file(text file)");
        out.println("         '-O' - Absolute path for output directory");
    }

    public void stdoutOutputAlreadyExists(String message) {
        out.println(message);
        out.println("Try again. Output filenames have epochsecond in name, which should minimize this problem.");
    }

    public void stdoutFileNotAccessible(String message) {
        out.println("Output file not accessible. Might be a fluke. Check folder permissions. ");
        out.println(message);
    }
}
