package icd0004.myExceptions;

public class FileFormatException extends Exception {
    public FileFormatException(String message) {
        super(message);
    }
}

