package icd0004.openWeatherMap;

import icd0004.myExceptions.CityNotFoundException;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.time.temporal.ChronoUnit.SECONDS;

public class OWMApi {

    public OWMApi() {

    }

    public String get(String url) throws IOException, CityNotFoundException, InterruptedException {
        //https://www.baeldung.com/java-9-http-client
        var client = HttpClient.newHttpClient();

        var request = HttpRequest.newBuilder(
                URI.create(url))
                .version(HttpClient.Version.HTTP_1_1)
                .timeout(Duration.of(5, SECONDS))
                .GET()
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        int statusCode = getResponseCode(response.body());
        if (statusCode == 404) {
            throw new CityNotFoundException("City not found!");
        }
        return response.body();
    }

    private int getResponseCode(String body) {
        Pattern p = Pattern.compile("(?<=\"cod\":\")\\d{3}");
        Matcher m = p.matcher(body);
        if (m.find()) {
            String match = m.group();
            try {
                return Integer.parseInt(match);
            } catch (NumberFormatException e) {
                return -1;
            }
        } else {
            return -1;
        }
    }
}
