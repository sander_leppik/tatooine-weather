package icd0004.openWeatherMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class OWMRequestBuilder {

    private static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    private static final String CURRENT_WEATHER_API = "weather"; //default
    private static final String WEATHER_FORECAST_API = "forecast";

    private static final String UNITS = "units";
    private static final String METRIC = "metric"; //default
    private static final String IMPERIAL = "imperial";
    private static final String STANDARD = "standard";

    private static final String APPID = "appid";
    private static final String CITY = "q";
    private static final String LAT = "lat";
    private static final String LON = "lon";


    private final String APIKEY;

    private final Map<String, String> parametersMap = new HashMap<>();
    private String apiSpecifier;


    public OWMRequestBuilder(String cityName) {
        APIKEY = loadApiKey();
        setParameter(CITY, cityName.replaceAll(" ", "%20"));
    }

    public OWMRequestBuilder(String latitude, String longitude) {
        APIKEY = loadApiKey();
        setParameter(LAT, latitude);
        setParameter(LON, longitude);
    }

    public String build() {

        if (builderSetupIsValid()) {
            StringBuilder sb = new StringBuilder(BASE_URL)
                    .append(apiSpecifier)
                    .append(String.format("?%s=%s&", APPID, APIKEY));

            for (Map.Entry<String, String> entry : parametersMap.entrySet()) {
                if (entry.getValue() != null) {
                    sb.append(String.format("%s=%s&", entry.getKey(), entry.getValue()));
                }
            }

            String requestUrl = sb.toString();

            requestUrl = requestUrl.endsWith("&") ?
                    requestUrl.substring(0, requestUrl.length() - 1) : requestUrl;
            return requestUrl;
        } else {
            throw new IllegalStateException(
                    "OWMRequestBuilder.weatherForecast or OWMRequestBuilder.currentWeather " +
                            "must be set for the builder");
        }
    }

    private boolean builderSetupIsValid() {
        return requiredParametersAreSet() && locationRquirementsAreSet();
    }

    private boolean locationRquirementsAreSet() {
        return parametersMap.containsKey(CITY) && parametersMap.get(CITY) != null ||
                parametersMap.containsKey(LAT) && parametersMap.get(LAT) != null &&
                        parametersMap.containsKey(LON) && parametersMap.get(LON) != null;
    }

    private boolean requiredParametersAreSet() {
        return apiSpecifier != null;
    }

    public OWMRequestBuilder imperial() {
        setParameter(UNITS, IMPERIAL);
        return this;
    }

    public OWMRequestBuilder metric() {
        setParameter(UNITS, METRIC);
        return this;
    }

    public OWMRequestBuilder weatherForecast() {
        this.apiSpecifier = WEATHER_FORECAST_API;
        return this;
    }

    public OWMRequestBuilder currentWeather() {
        this.apiSpecifier = CURRENT_WEATHER_API;
        return this;
    }

    private void setParameter(String key, String value) {
        if (parametersMap.containsKey(key)) {
            parametersMap.replace(key, value);
        } else {
            parametersMap.put(key, value);
        }
    }

    private String loadApiKey() {
        Properties properties = new Properties();
        try (InputStream inputStream = getClass()
                .getClassLoader()
                .getResourceAsStream("openweathermap.properties")) {
            if (inputStream != null) {
                properties.load(inputStream);
                return properties.getProperty("apiKey");

            } else {
                throw new RuntimeException("OWM properties file not found");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}