package icd0004.service;

import icd0004.model.Report;
import icd0004.model.WeatherReport;
import icd0004.myExceptions.CityNotFoundException;
import icd0004.openWeatherMap.OWMApi;
import icd0004.openWeatherMap.OWMRequestBuilder;
import icd0004.util.JSONParser;
import icd0004.util.WeatherProcessor;

import java.io.IOException;
import java.util.List;

public class WeatherServiceImpl implements IWeatherService {


    private OWMApi api = new OWMApi();

    public WeatherServiceImpl() {

    }

    @Override
    public Report getFullReport(String cityName) throws InterruptedException, CityNotFoundException, IOException {
        WeatherReport current = getCurrentWeather(cityName);
        List<WeatherReport> forecast = WeatherProcessor
                .consolidateWeatherForecast(getDetailedWeatherForecast(cityName));
        return new Report(current, forecast);
    }

    @Override
    public Report getSimpleReport(String cityName) throws IOException, InterruptedException, CityNotFoundException {
        WeatherReport currentWeather = getCurrentWeather(cityName);

        return new Report(currentWeather);
    }

    @Override
    public WeatherReport getCurrentWeather(String latitude, String longitude) throws IOException, InterruptedException, CityNotFoundException {
        String request = new OWMRequestBuilder(latitude, longitude)
                .metric()
                .currentWeather()
                .build();

        String response = api.get(request);
        WeatherReport report = new JSONParser().parseCurrentWeather(response);
        report.setTemperatureUnit("Celsius");

        return report;
    }

    @Override
    public WeatherReport getCurrentWeather(String cityName) throws IOException, InterruptedException, CityNotFoundException {
        String request = new OWMRequestBuilder(cityName)
                .metric()
                .currentWeather()
                .build();

        String response = api.get(request);
        WeatherReport report = new JSONParser().parseCurrentWeather(response);
        report.setTemperatureUnit("Celsius");

        return report;
    }

    @Override
    public List<WeatherReport> getDetailedWeatherForecast(String latitude, String longitude) throws IOException, InterruptedException, CityNotFoundException {
        String request = new OWMRequestBuilder(latitude, longitude)
                .metric()
                .weatherForecast()
                .build();

        String response = api.get(request);

        List<WeatherReport> list = new JSONParser().parseWeatherForecast(response);
        list.forEach(e -> e.setTemperatureUnit("Celsius"));

        return list;
    }

    @Override
    public List<WeatherReport> getDetailedWeatherForecast(String cityName) throws IOException, InterruptedException, CityNotFoundException {
        String request = new OWMRequestBuilder(cityName)
                .metric()
                .weatherForecast()
                .build();

        String response = api.get(request);

        List<WeatherReport> list = new JSONParser().parseWeatherForecast(response);
        list.forEach(e -> e.setTemperatureUnit("Celsius"));

        return list;
    }

}
