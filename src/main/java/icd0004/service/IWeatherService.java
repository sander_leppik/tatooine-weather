package icd0004.service;

import icd0004.model.Report;
import icd0004.model.WeatherReport;
import icd0004.myExceptions.CityNotFoundException;

import java.io.IOException;
import java.util.List;

public interface IWeatherService {
    WeatherReport getCurrentWeather(String latitude, String longitude) throws IOException, InterruptedException, IllegalAccessException, CityNotFoundException;

    WeatherReport getCurrentWeather(String cityName) throws IOException, InterruptedException, CityNotFoundException;

    List<WeatherReport> getDetailedWeatherForecast(String latitude, String longitude) throws IOException, InterruptedException, CityNotFoundException;

    List<WeatherReport> getDetailedWeatherForecast(String cityName) throws IOException, InterruptedException, CityNotFoundException;

    Report getFullReport(String cityName) throws IOException, InterruptedException, CityNotFoundException;

    Report getSimpleReport(String city) throws IOException, InterruptedException, CityNotFoundException;
}
