package icd0004.engine;

import icd0004.model.Report;
import icd0004.myExceptions.CityNotFoundException;
import icd0004.myExceptions.FileFormatException;
import icd0004.service.WeatherServiceImpl;
import icd0004.util.FileUtil;
import icd0004.util.JSONParser;
import icd0004.util.StandardOutputPrinter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.LinkedList;
import java.util.List;

public class App {

    private static final String FULL_WEATHER_REPORT = "-FULL";
    private static final String SIMPLE_WEATHER_REPORT = "-SIMPLE";
    private static String input = "";
    private static String output = "";
    private static String city = "";
    private static String operation = "";

    private static final StandardOutputPrinter printer = new StandardOutputPrinter();
    private static WeatherServiceImpl service = new WeatherServiceImpl();

    public App(WeatherServiceImpl service) {

        App.service = service;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        setArguments(args);
        if (!operation.equals("")) {
            if (!city.equals("") && !input.equals("") || !city.equals("") && !output.equals("")) {
                printer.stdoutStandardOrFile();
            } else if (!city.equals("")) {
                standardOutput(operation, city);
            } else if (!input.equals("") && !output.equals("")) {
                fileOutput(operation, input, output);
            } else {
                printer.stdoutArgumentsMissing();
            }
        } else {
            printer.stdoutArgumentsMissing();
        }
        cleanArguments();
    }

    private static void setArguments(String[] args) {
        if (args.length > 2) {
            for (int i = 0; i < args.length; i++) {
                if (args[i].equalsIgnoreCase("-H")) {
                    printer.stdoutHelp();
                } else if (args[i].equalsIgnoreCase(FULL_WEATHER_REPORT)) {
                    operation = FULL_WEATHER_REPORT;
                } else if (args[i].equalsIgnoreCase(SIMPLE_WEATHER_REPORT)) {
                    operation = SIMPLE_WEATHER_REPORT;
                } else if (args[i].equalsIgnoreCase("-city")) {
                    city = args[++i];
                } else if (args[i].equalsIgnoreCase("-F")) {
                    input = args[++i];
                } else if (args[i].equalsIgnoreCase("-O")) {
                    output = args[++i];
                } else {
                    printer.stdoutNoSuchArgument(args[i]);
                }
            }
        } else {
            printer.stdoutArgumentsMissing();
        }
    }

    private static void standardOutput(String operation, String city) throws IOException, InterruptedException {
        Report report = new Report();

        try {
            if (operation.equals(FULL_WEATHER_REPORT)) {
                report = service.getFullReport(city);
            } else if (operation.equals(SIMPLE_WEATHER_REPORT)) {
                report = service.getSimpleReport(city);
            }
            String output = JSONParser.writeAsString(report);
            printer.stdout(output);
        } catch (CityNotFoundException e) {
            printer.stdoutCityNotFound(city);
        }

    }

    private static void fileOutput(String operation, String input, String output) throws InterruptedException {
        List<String> cities = new LinkedList<>();
        Report report;

        try {
            cities = FileUtil.readInput(input);
        } catch (FileNotFoundException e) {
            printer.stdoutInputNotFound(input);
        } catch (FileFormatException e) {
            printer.stdoutIllegalFileFormat(e);
        } catch (IOException e) {
            printer.stdoutFileNotAccessible(e.getMessage());
        }
        if (cities.size() > 0) {

            for (String city : cities) {
                if (operation.equals(FULL_WEATHER_REPORT)) {
                    try {
                        report = service.getFullReport(city);
                        FileUtil.writeReportToFile(report, output);

                    } catch (CityNotFoundException e) {
                        printer.stdoutCityNotFound(city);
                    } catch (FileAlreadyExistsException e) {
                        printer.stdoutOutputAlreadyExists(e.getMessage());
                    } catch (IOException e) {
                        printer.stdoutFileNotAccessible(e.getMessage());
                    }
                } else if (operation.equals(SIMPLE_WEATHER_REPORT)) {
                    try {
                        report = service.getSimpleReport(city);
                        FileUtil.writeReportToFile(report, output);

                    } catch (CityNotFoundException e) {
                        printer.stdoutCityNotFound(city);
                    } catch (FileAlreadyExistsException e) {
                        printer.stdoutOutputAlreadyExists(e.getMessage());
                    } catch (IOException e) {
                        printer.stdoutFileNotAccessible(e.getMessage());
                    }
                }
            }
        }
    }

    public String getStdout() {
        return printer.getLastPrint();
    }

    private static void cleanArguments() {
        input = "";
        output = "";
        city = "";
        operation = "";
    }
}
