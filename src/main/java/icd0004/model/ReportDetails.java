package icd0004.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportDetails {
    public String city;
    public String coordinates;
    public String temperatureUnit;

    public void setCoordinates(Double lat, Double lon) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(' ');

        DecimalFormat f = new DecimalFormat("##.##", otherSymbols);

        this.coordinates = String.format("%s, %s", f.format(lat), f.format(lon));
    }
}