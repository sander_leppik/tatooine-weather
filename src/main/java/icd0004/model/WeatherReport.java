package icd0004.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherReport {

    @JsonAlias("dt")
    private Long date;
    private Double temperature;
    private Integer humidity;
    private Integer pressure;

    @JsonIgnore
    private String temperatureUnit;
    @JsonIgnore
    private String name;
    @JsonIgnore
    private Double lon;
    @JsonIgnore
    private Double lat;

    @JsonProperty("main")
    private void weatherReportDeserializer(Map<String, Object> main) {

        if (main.get("temp") instanceof Double) {
            this.temperature = (Double) main.get("temp");
        } else if (main.get("temp") instanceof Integer) {
            this.temperature = ((Integer) main.get("temp")).doubleValue();
        }
        this.humidity = (Integer) main.get("humidity");
        this.pressure = (Integer) main.get("pressure");
    }

    public void setLon(Double input){
        this.lon = Math.round(input * 100.0) / 100.0;
    }

    public void setLat(Double input){
        this.lat = Math.round(input * 100.0) / 100.0;
    }

    @JsonProperty("date")
    public String getDateAsString() {
        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        ZonedDateTime zdt = ZonedDateTime.ofInstant(Instant.ofEpochSecond(date), ZoneId.of("UTC"));

        return f.format(zdt);
    }

    @JsonProperty("temperature")
    public Integer getRoundedTemperature() {
        return Math.toIntExact(Math.round(temperature));
    }

}