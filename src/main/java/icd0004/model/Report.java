package icd0004.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Report {
    public ReportDetails reportDetails;
    public WeatherReport currentWeatherReport;
    public List<WeatherReport> forecastReports;

    public Report(WeatherReport current, List<WeatherReport> forecast) {
        if (locationsEqual(current, forecast)) {
            setDetails(current);
        }
        if (validWeatherReports(current, forecast)) {
            setWeatherReports(current, forecast);
        }
    }

    public Report(WeatherReport currentWeatherReport) {
        setDetails(currentWeatherReport);
        this.currentWeatherReport = currentWeatherReport;
    }

    private void setWeatherReports(WeatherReport current, List<WeatherReport> forecast) {
        currentWeatherReport = current;
        forecastReports = new LinkedList<>();

        ZonedDateTime tempDt = getStartOfDayUnix(current.getDate());
        for (int i = 1; i < 4; i++) {
            tempDt = tempDt.plusDays(1);
            for (WeatherReport report : forecast) {
                if (report.getDate() == tempDt.toEpochSecond()) {
                    forecastReports.add(report);
                }
            }
        }
    }

    private boolean validWeatherReports(WeatherReport current, List<WeatherReport> forecast) {
        ZonedDateTime currentDt = getStartOfDayUnix(current.getDate());
        for (int i = 1; i < 4; i++) {
            ZonedDateTime tempDt = currentDt.plusDays(i);
            if (!forecastListContainsDate(tempDt, forecast)) {
                return false;
            }
        }
        return true;
    }

    private boolean forecastListContainsDate(ZonedDateTime tempDt, List<WeatherReport> forecast) {
        for (WeatherReport report : forecast) {
            if (report.getDate() == tempDt.toEpochSecond()) return true;
        }
        return false;
    }

    private static ZonedDateTime getStartOfDayUnix(long dt) {
        ZonedDateTime zdt = ZonedDateTime.ofInstant(Instant.ofEpochSecond(dt), ZoneId.of("UTC"));

        return ZonedDateTime.of(zdt.getYear(), zdt.getMonthValue(), zdt.getDayOfMonth(),
                0, 0, 0, 0, ZoneId.of("UTC"));
    }


    private boolean locationsEqual(WeatherReport current, List<WeatherReport> forecast) {
        for (WeatherReport report : forecast) {
            if (!current.getName().equals(report.getName())) {
                throw new RuntimeException("City names don't match!");
            } else if (!current.getLat().equals(report.getLat()) && !current.getLon().equals(report.getLon())) {
                System.out.println("Current: " + current.getLat() + ", " + current.getLon() +
                        "| Forecast: " + report.getLat() + ", " + report.getLon());
                throw new RuntimeException("Coordinates don't match!");
            }
        }
        return true;
    }

    private void setDetails(WeatherReport report) {
        this.reportDetails = new ReportDetails();
        reportDetails.setCity(report.getName());
        reportDetails.setCoordinates(report.getLat(), report.getLon());
        reportDetails.setTemperatureUnit(report.getTemperatureUnit());
    }
}
