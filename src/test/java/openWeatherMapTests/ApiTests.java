package openWeatherMapTests;

import icd0004.myExceptions.CityNotFoundException;
import icd0004.openWeatherMap.OWMApi;
import icd0004.openWeatherMap.OWMRequestBuilder;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class ApiTests {

    @Test
    public void shouldGetStatusCode200WhenValidRequest() throws IOException, InterruptedException, CityNotFoundException {
        String cityName = "Tartu";
        String request = metricCurrentWeatherRequest(cityName);
        String expectedString = "\"cod\":200";

        String response = new OWMApi().get(request);
        assertThat(response, containsString(expectedString));
    }

    @Test
    public void shouldGetWeatherInfoForRequestedCity() throws IOException, InterruptedException, CityNotFoundException {
        String cityName = "Tallinn";
        String request = metricCurrentWeatherRequest(cityName);
        String expectedString = String.format("\"name\":\"%s\"", cityName);

        String response = new OWMApi().get(request);
        assertThat(response, containsString(expectedString));
    }

    @Test(expected = CityNotFoundException.class)
    public void shouldGetErrorCode404WhenCityNotFound() throws IOException, InterruptedException, CityNotFoundException {
        String cityName = "WololooooWololooooWLolWOloooooWOololooo";
        String request = metricCurrentWeatherRequest(cityName);

        String response = new OWMApi().get(request);

    }

    @Test
    public void shouldGetWeatherForecastForCity() throws IOException, InterruptedException, CityNotFoundException {
        String cityName = "Tartu";
        String request = metricWeatherForecastRequest(cityName);
        String expectedString = String.format("\"name\":\"%s\"", cityName);

        String response = new OWMApi().get(request);

        assertThat(response, containsString(expectedString));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenForecastOrCurrentWeatherNotSet() throws IOException, CityNotFoundException, InterruptedException {
        String cityName = "Jõhvi";
        String request = new OWMRequestBuilder(cityName).build();
        new OWMApi().get(request);
    }

    public String metricCurrentWeatherRequest(String cityName) {
        return new OWMRequestBuilder(cityName)
                .metric()
                .currentWeather()
                .build();
    }

    public String metricWeatherForecastRequest(String cityName) {
        return new OWMRequestBuilder(cityName)
                .weatherForecast()
                .metric()
                .build();
    }
}
