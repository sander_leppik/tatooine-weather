package utilTests;

import icd0004.model.Report;
import icd0004.model.ReportDetails;
import icd0004.model.WeatherReport;
import icd0004.myExceptions.FileFormatException;
import icd0004.util.FileUtil;
import org.junit.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class FileUtilTests {

    private static String inputOnePath = "src/test/resources/inputSample1.txt";
    private static String outputSamplePath = "src/test/resources/FullReportSample.json";

    @Before
    @After
    public void cleanup() {
        File directoryPath = new File("src/test/resources/temp");
        File[] fileList = directoryPath.listFiles();
        for (File file : fileList) {
            file.delete();
        }
    }

    @Test
    public void shouldReadInputFromFile() throws IOException, FileFormatException {
        String inputPath = inputOnePath;
        String expected = "Tartu";

        String actual = FileUtil.readInput(inputPath).get(0);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldWriteOutputToFile() throws IOException {
        String outputPath = "src/test/resources/temp/";
        String expected = readFile(outputSamplePath).replaceAll("\\s", "");
        Report report = getFullReportSample();

        FileUtil.writeReportToFile(report, outputPath);
        String actual = readFile(getFileFromOutputDir(outputPath)).replaceAll("\\s", "");

        Assert.assertEquals(expected, actual);
    }

    private String getFileFromOutputDir(String dirPath) {
        File dir = new File(dirPath);
        File[] fileList = dir.listFiles();
        if (fileList.length == 1) {
            return fileList[0].getAbsolutePath();
        } else {
            throw new RuntimeException("Output directory should have only 1 file, but has " + fileList.length);
        }
    }

    private String readFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    private Report getFullReportSample() {
        WeatherReport currentWeather = getWeatherReportSample();
        List<WeatherReport> weatherForecast = getWeatherForecastSample();
        Report report = new Report(currentWeather, weatherForecast);

        return report;
    }

    private Report getSimpleReportSample(){
        WeatherReport currentWeather = getWeatherReportSample();
        Report report = new Report(currentWeather);

        return report;
    }

    private WeatherReport getWeatherReportSample(){
        WeatherReport report = new WeatherReport(
                1606851270L, 666.1, 69, 1337,
                "Celsius", "Tartu", 26.73, 58.38);

        return report;
    }

    private List<WeatherReport> getWeatherForecastSample(){
        WeatherReport forecast1 = new WeatherReport(1606867200L, -2.3175, 96, 1030,
                "Celsius", "Tartu", 26.73, 58.38);
        WeatherReport forecast2 = new WeatherReport(1606953600L, -1.86875, 95, 1023,
                "Celsius", "Tartu", 26.73, 58.38);
        WeatherReport forecast3 = new WeatherReport(1607040000L, 1.10875, 90, 1019,
                "Celsius", "Tartu", 26.73, 58.38);

        List<WeatherReport> forecastList = new LinkedList<>();
        forecastList.add(forecast1);
        forecastList.add(forecast2);
        forecastList.add(forecast3);

        return forecastList;
    }
}
