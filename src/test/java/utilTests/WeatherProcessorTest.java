package utilTests;

import icd0004.model.WeatherReport;
import icd0004.util.JSONParser;
import icd0004.util.WeatherProcessor;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class WeatherProcessorTest {
    private static String SAMPLE_FORECAST_JSON = "src/test/resources/OWMFiveDayForecastSample.json";

    @Test
    public void shouldConsolidateDetailedForecastToSimple() throws IOException {
        String forecastJSON = new String(Files.readAllBytes(Paths.get(SAMPLE_FORECAST_JSON)));

        List<WeatherReport> originalList = new JSONParser().parseWeatherForecast(forecastJSON);
        List<WeatherReport> processedList = WeatherProcessor.consolidateWeatherForecast(originalList);

        Assert.assertTrue(processedList.size() == 6);
    }

    @Test
    public void shouldAverageData() throws IOException {
        String forecastJSON = new String(Files.readAllBytes(Paths.get(SAMPLE_FORECAST_JSON)));
        WeatherReport expectedReport = new WeatherReport(1606953600L, -1.86875, 95, 1023,"Celsius",  "Tartu", 26.73, 58.38);

        List<WeatherReport> originalList = new JSONParser().parseWeatherForecast(forecastJSON);
        List<WeatherReport> processedList = WeatherProcessor.consolidateWeatherForecast(originalList);

        Assert.assertTrue(processedList.contains(expectedReport));
    }


}
