package utilTests;

import com.fasterxml.jackson.core.JsonProcessingException;
import icd0004.model.Report;
import icd0004.model.WeatherReport;
import icd0004.util.JSONParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class JSONParserTests {


    private String currentJSON;
    private String forecastJSON;
    private final JSONParser parser = new JSONParser();
    String currentPath = "src/test/resources/OWMCurrentWeatherSample.json";
    String forecastPath = "src/test/resources/OWMFiveDayForecastSample.json";
    String fullReportSample = "src/test/resources/FullReportSample.json";
    String simpleReportSample = "src/test/resources/SimpleReportSample.json";

    @Before
    public void setup() throws IOException {
        currentJSON = new String(Files.readAllBytes(Paths.get(currentPath)));
        forecastJSON = new String(Files.readAllBytes(Paths.get(forecastPath)));
    }

    @Test
    public void shouldWriteFullReportToString() throws IOException {
        Report report = getFullReportSample();
        String expected = new String(Files.readAllBytes(Paths.get(fullReportSample))).replaceAll("\\s", "");

        String actual = JSONParser.writeAsString(report).replaceAll("\\s", "");

        Assert.assertTrue(expected.length() != 0 && actual.length() != 0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldWriteSimpleReportToString() throws IOException {
        Report report = getSimpleReportSample();
        String expected = new String(Files.readAllBytes(Paths.get(simpleReportSample))).replaceAll("\\s", "");

        String actual = JSONParser.writeAsString(report).replaceAll("\\s", "");

        Assert.assertTrue(expected.length() != 0 && actual.length() != 0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldParseCurrentWeatherJSON() throws JsonProcessingException {
        WeatherReport expected = new WeatherReport(1606851270L, 666.1, 69, 1337, "Celsius", "Tartu", 26.73, 58.38);

        WeatherReport actual = parser.parseCurrentWeather(currentJSON);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldParseWeatherForecastJSON() throws IOException {
        WeatherReport expectedReport1 = new WeatherReport(1606856400L, -0.13, 92, 1029, "Celsius", "Tartu", 26.73, 58.38);
        WeatherReport expectedReport2 = new WeatherReport(1607040000L, -1.06, 96, 1020, "Celsius", "Tartu", 26.73, 58.38);

        List<WeatherReport> actualReports = parser.parseWeatherForecast(forecastJSON);

        Assert.assertTrue(actualReports.contains(expectedReport1));
        Assert.assertTrue(actualReports.contains(expectedReport2));
        Assert.assertEquals(40, actualReports.size());
    }

    private Report getFullReportSample() {
        WeatherReport currentWeather = getWeatherReportSample();
        List<WeatherReport> weatherForecast = getWeatherForecastSample();
        Report report = new Report(currentWeather, weatherForecast);

        return report;
    }

    private Report getSimpleReportSample(){
        WeatherReport currentWeather = getWeatherReportSample();
        Report report = new Report(currentWeather);

        return report;
    }

    private WeatherReport getWeatherReportSample(){
        WeatherReport report = new WeatherReport(
                1606851270L, 666.1, 69, 1337,
                "Celsius", "Tartu", 26.73, 58.38);

        return report;
    }

    private List<WeatherReport> getWeatherForecastSample(){
        WeatherReport forecast1 = new WeatherReport(1606867200L, -2.3175, 96, 1030,
                "Celsius", "Tartu", 26.73, 58.38);
        WeatherReport forecast2 = new WeatherReport(1606953600L, -1.86875, 95, 1023,
                "Celsius", "Tartu", 26.73, 58.38);
        WeatherReport forecast3 = new WeatherReport(1607040000L, 1.10875, 90, 1019,
                "Celsius", "Tartu", 26.73, 58.38);

        List<WeatherReport> forecastList = new LinkedList<>();
        forecastList.add(forecast1);
        forecastList.add(forecast2);
        forecastList.add(forecast3);

        return forecastList;
    }

}
