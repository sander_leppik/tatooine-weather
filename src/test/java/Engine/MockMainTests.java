package Engine;

import icd0004.engine.App;
import icd0004.myExceptions.CityNotFoundException;
import icd0004.openWeatherMap.OWMApi;
import icd0004.service.WeatherServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MockMainTests {

    String currentJSON;
    String forecastJSON;
    private String currentPath = "src/test/resources/OWMCurrentWeatherSample.json";
    private String forecastPath = "src/test/resources/OWMFiveDayForecastSample.json";
    private String fullReportSamplePath = "src/test/resources/FullReportSample.json";
    private String simpleReportSamplePath = "src/test/resources/SimpleReportSample.json";
    private String inputOnePath = "src/test/resources/inputSample1.txt";
    private String inputFivePath = "src/test/resources/inputSample5.txt";

    //!!!! Should be temp folder, because of the automatic cleanup!!!!
    private static String outputDirectory = "src/test/resources/temp/";

    App app;

    @Mock
    OWMApi api;

    @InjectMocks
    WeatherServiceImpl service;

    @Before
    public void setup() throws IOException, InterruptedException, CityNotFoundException {

        MockitoAnnotations.openMocks(this);
        currentJSON = new String(Files.readAllBytes(Paths.get(currentPath)));
        forecastJSON = new String(Files.readAllBytes(Paths.get(forecastPath)));
        app = new App(service);
    }


    @After
    public void cleanup() {
        File directoryPath = new File(outputDirectory);
        File[] fileList = directoryPath.listFiles();
        for (File file : fileList) {
            file.delete();
        }
    }

    @Test
    public void shouldReadFromFileAndWriteToFile() throws InterruptedException, IOException, CityNotFoundException {
        when(api.get(Mockito.contains("api.openweathermap.org/data/2.5/weather"))).thenReturn(currentJSON);
        when(api.get(Mockito.contains("api.openweathermap.org/data/2.5/forecast"))).thenReturn(forecastJSON);
        String expected = readFile(fullReportSamplePath);
        String[] args = new String[]{"-FULL", "-F", inputOnePath, "-o", outputDirectory};

        app.main(args);

        String actual = readFile(getFileFromOutputDir(outputDirectory));

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldWriteFullReportToStandardOutput() throws IOException, InterruptedException, CityNotFoundException {
        when(api.get(Mockito.contains("api.openweathermap.org/data/2.5/weather"))).thenReturn(currentJSON);
        when(api.get(Mockito.contains("api.openweathermap.org/data/2.5/forecast"))).thenReturn(forecastJSON);
        String expected = readFile(fullReportSamplePath);
        String[] args = new String[]{"-FULL", "-city", "Tartu"};

        app.main(args);
        String actual = app.getStdout().replaceAll("\\s", "");

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldWriteSimpleReportToStandardOutput() throws IOException, InterruptedException, CityNotFoundException {
        when(api.get(Mockito.contains("api.openweathermap.org/data/2.5/weather"))).thenReturn(currentJSON);
        String expected = readFile(simpleReportSamplePath);
        String[] args = new String[]{"-simple", "-city", "Tartu"};

        app.main(args);

        String actual = app.getStdout().replaceAll("\\s", "");

        Assert.assertEquals(expected, actual);
    }

    private String getFileFromOutputDir(String dirPath) {
        File dir = new File(dirPath);
        File[] fileList = dir.listFiles();
        if (fileList.length == 1) {
            return fileList[0].getAbsolutePath();
        } else {
            return null;
        }
    }

    private String readFile(String path) throws IOException {
        String result = new String(Files.readAllBytes(Paths.get(path)));
        return result.replaceAll("\\s", ""); //Whitespaces don't count in JSON
    }



}
