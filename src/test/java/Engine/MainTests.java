package Engine;

import icd0004.engine.App;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MainTests {

    String currentJSON;
    String forecastJSON;
    String currentPath = "src/test/resources/OWMCurrentWeatherSample.json";
    String forecastPath = "src/test/resources/OWMFiveDayForecastSample.json";
    String outputSamplePath = "src/test/resources/FullReportSample.json";

    private  String inputOnePath = "src/test/resources/inputSample1.txt";
    private  String inputFivePath = "src/test/resources/inputSample5.txt";
    private  String nonExistingCities = "src/test/resources/NonExistingCities.txt";
    //!!!! Should be temp folder, because of automatic cleanup!!!!
    private  String outputDirectory = "src/test/resources/temp/";


    @Before
    public void createTempFolderIfNotExists() {
        File file = new File(outputDirectory);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    @After
    public void cleanup() {
        File directoryPath = new File(outputDirectory);
        File[] fileList = directoryPath.listFiles();
        for (File file : fileList) {
            file.delete();
        }

    }

    @Test
    public void shouldWriteReportForAllCities() throws IOException, InterruptedException {
        List<String> cities = getRequestedCities(inputFivePath);
        String[] args = new String[]{"-FULL", "-F", inputFivePath, "-O", outputDirectory};

        App.main(args);

        for (String city : cities) {
            //Has full report a.k.a no nulls in report.
            Assert.assertTrue(hasFullReportOfInOutputFiles(city));
        }

    }

    @Test
    public void shouldNotWriteFileForNonExistingCity() throws InterruptedException, IOException {
        List<String> cities = getRequestedCities(nonExistingCities);
        String[] args = new String[]{"-FULL", "-F", nonExistingCities, "-O", outputDirectory};

        App.main(args);

        Assert.assertTrue(noCityNamesInOutputDir(cities, outputDirectory));
    }

    private boolean noCityNamesInOutputDir(List<String> cities, String outputDirectory) throws IOException {
        List<String> outputFiles = readAllFilesToList(outputDirectory);
        for (String outputFile : outputFiles) {
            for (String city : cities) {
                if (outputFile.contains(city)) {
                    return false;
                }

            }
        }
        return true;
    }


    private boolean hasFullReportOfInOutputFiles(String city) throws IOException {
        List<String> outputs = readAllFilesToList(outputDirectory);
        for (String output : outputs) {
            if (output.contains(city) && !output.contains("null")) {
                return true;
            }
        }
        return false;
    }

    private List<String> readAllFilesToList(String outputDirectory) throws IOException {
        List<String> outputFiles = new LinkedList<>();

        File directoryPath = new File(outputDirectory);
        File[] fileList = directoryPath.listFiles();
        for (File file : fileList) {
            outputFiles.add(readFile(file.getAbsolutePath()));
        }

        return outputFiles;
    }

    private List<String> getRequestedCities(String inputPath) throws IOException {
        File file = new File(inputPath);
        Scanner sc = new Scanner(file, StandardCharsets.UTF_8);

        List<String> cities = new LinkedList<>();
        while (sc.hasNextLine()) {
            cities.add(sc.nextLine());
        }

        return cities;
    }

    private String readFile(String path) throws IOException {
        String result = new String(Files.readAllBytes(Paths.get(path)));
        return result.replaceAll("\\s", ""); //Whitespaces don't count in JSON
    }

}
