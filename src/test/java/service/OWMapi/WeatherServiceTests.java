package service.OWMapi;

import icd0004.model.Report;
import icd0004.myExceptions.CityNotFoundException;
import icd0004.model.WeatherReport;
import icd0004.service.WeatherServiceImpl;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertThat;
import static service.OWMapi.WeatherServiceTests.AllFieldsSet.hasAllFieldsNotNull;

public class WeatherServiceTests {

    WeatherServiceImpl service = new WeatherServiceImpl();

    @Test
    public void shouldGetCurrentWeatherReportByName() throws IOException, InterruptedException, CityNotFoundException {
        String cityName = "Tallinn";

        WeatherReport report = service.getCurrentWeather(cityName);

        assertThat(report, hasAllFieldsNotNull());
    }

    @Test
    public void shouldGetFeatherForecastReportByName() throws IOException, InterruptedException, CityNotFoundException {
        String cityName = "Tartu";

        List<WeatherReport> weatherReportList = service.getDetailedWeatherForecast(cityName);

        for (WeatherReport weatherReport : weatherReportList) {
            assertThat(weatherReport, hasAllFieldsNotNull());
        }
    }

    @Test
    public void shouldGetCurrentWeatherReportByCoordinates() throws InterruptedException, IOException, CityNotFoundException {
        String lat = "58.38";
        String lon = "26.73";
        WeatherReport report = service.getCurrentWeather(lat, lon);

        assertThat(report, hasAllFieldsNotNull());
    }

    @Test
    public void shouldGetWeatherForecastReportByCoordinates() throws IOException, InterruptedException, CityNotFoundException {
        String lat = "58.38";
        String lon = "26.73";

        List<WeatherReport> weatherReportList = service.getDetailedWeatherForecast(lat, lon);

        for (WeatherReport weatherReport : weatherReportList) {
            assertThat(weatherReport, hasAllFieldsNotNull());
        }
    }

    private Report getFullReportSample() {
        WeatherReport currentWeather = getWeatherReportSample();
        List<WeatherReport> weatherForecast = getWeatherForecastSample();

        return new Report(currentWeather, weatherForecast);
    }

    private Report getSimpleReportSample(){
        WeatherReport currentWeather = getWeatherReportSample();

        return new Report(currentWeather);
    }

    private WeatherReport getWeatherReportSample(){
        return  new WeatherReport(
                1606851270L, 666.1, 69, 1337,
                "Celsius", "Tartu", 26.73, 58.38);
    }

    private List<WeatherReport> getWeatherForecastSample(){
        WeatherReport forecast1 = new WeatherReport(1606867200L, -2.3175, 96, 1030,
                "Celsius", "Tartu", 26.73, 58.38);
        WeatherReport forecast2 = new WeatherReport(1606953600L, -1.86875, 95, 1023,
                "Celsius", "Tartu", 26.73, 58.38);
        WeatherReport forecast3 = new WeatherReport(1607040000L, 1.10875, 90, 1019,
                "Celsius", "Tartu", 26.73, 58.38);

        List<WeatherReport> forecastList = new LinkedList<>();
        forecastList.add(forecast1);
        forecastList.add(forecast2);
        forecastList.add(forecast3);

        return forecastList;
    }

    public static class AllFieldsSet extends TypeSafeMatcher {

        @Override
        protected boolean matchesSafely(Object item) {
            for (Field field : item.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    if (field.get(item) == null) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            return true;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("all fields are not null");
        }

        public static Matcher hasAllFieldsNotNull() {
            return new AllFieldsSet();
        }
    }
}
