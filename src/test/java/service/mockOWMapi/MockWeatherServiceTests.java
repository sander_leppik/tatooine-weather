package service.mockOWMapi;


import icd0004.model.Report;
import icd0004.model.WeatherReport;
import icd0004.myExceptions.CityNotFoundException;
import icd0004.openWeatherMap.OWMApi;
import icd0004.service.WeatherServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MockWeatherServiceTests {

    private String currentJSON;
    private String forecastJSON;
    private WeatherReport currentWeather;
    private WeatherReport expectedForecastReport1;
    private WeatherReport expectedForecastReport2;
    private List<WeatherReport> weatherForecast;
    private Report expectedReport = new Report();
    private final String currentPath = "src/test/resources/OWMCurrentWeatherSample.json";
    private final String forecastPath = "src/test/resources/OWMFiveDayForecastSample.json";

    @Mock
    OWMApi api;

    @InjectMocks
    WeatherServiceImpl service;

    @Before
    public void setup() throws IOException {
        MockitoAnnotations.openMocks(this);

        currentJSON = new String(Files.readAllBytes(Paths.get(currentPath)));
        forecastJSON = new String(Files.readAllBytes(Paths.get(forecastPath)));

        currentWeather = getWeatherReportSample();
        weatherForecast = getWeatherForecastSample();
        expectedReport = new Report(currentWeather, weatherForecast);

        expectedForecastReport1 = new WeatherReport(1606856400L, -0.13, 92, 1029,"Celsius",  "Tartu", 26.73, 58.38);
        expectedForecastReport2 = new WeatherReport(1607040000L, -1.06, 96, 1020,"Celsius",  "Tartu", 26.73, 58.38);
    }


    @Test
    public void shouldReturnFullReport() throws IOException, InterruptedException, CityNotFoundException {
        when(api.get(Mockito.contains("api.openweathermap.org/data/2.5/forecast"))).thenReturn(forecastJSON);
        when(api.get(Mockito.contains("api.openweathermap.org/data/2.5/weather"))).thenReturn(currentJSON);

        Report report = service.getFullReport("Tartu");

        Assert.assertEquals(expectedReport, report);
    }

    @Test
    public void shouldReturnCurrentWeatherReportByCity() throws IOException, InterruptedException, CityNotFoundException {
        when(api.get(Mockito.anyString())).thenReturn(currentJSON);

        WeatherReport actualReport = service.getCurrentWeather("Tartu");

        Assert.assertEquals(currentWeather, actualReport);
    }

    @Test
    public void shouldReturnCurrentWeatherReportByCoordinates() throws InterruptedException, IOException, CityNotFoundException {
        when(api.get(Mockito.anyString())).thenReturn(currentJSON);

        WeatherReport actualReport = service.getCurrentWeather("26.73", "58.38");

        Assert.assertEquals(currentWeather, actualReport);
    }

    @Test
    public void shouldReturnWeatherForecastReportByCity() throws IOException, InterruptedException, CityNotFoundException {
        when(api.get(Mockito.anyString())).thenReturn(forecastJSON);
        List<WeatherReport> actualReports = service.getDetailedWeatherForecast("Tartu");

        Assert.assertTrue(actualReports.contains(expectedForecastReport1));
        Assert.assertTrue(actualReports.contains(expectedForecastReport2));
    }

    @Test
    public void shouldReturnWeatherForecastReportByCoordinates() throws IOException, InterruptedException, CityNotFoundException {
        when(api.get(Mockito.anyString())).thenReturn(forecastJSON);

        List<WeatherReport> actualReports = service.getDetailedWeatherForecast("26.73", "58.38");

        Assert.assertTrue(actualReports.contains(expectedForecastReport1));
        Assert.assertTrue(actualReports.contains(expectedForecastReport2));
    }

    private Report getFullReportSample() {
        WeatherReport currentWeather = getWeatherReportSample();
        List<WeatherReport> weatherForecast = getWeatherForecastSample();
        Report report = new Report(currentWeather, weatherForecast);

        return report;
    }

    private Report getSimpleReportSample() {
        WeatherReport currentWeather = getWeatherReportSample();
        Report report = new Report(currentWeather);

        return report;
    }

    private WeatherReport getWeatherReportSample() {
        WeatherReport report = new WeatherReport(
                1606851270L, 666.1, 69, 1337,
                "Celsius", "Tartu", 26.73, 58.38);

        return report;
    }

    private List<WeatherReport> getWeatherForecastSample() {
        WeatherReport forecast1 = new WeatherReport(1606867200L, -2.3175, 96, 1030,
                "Celsius", "Tartu", 26.73, 58.38);
        WeatherReport forecast2 = new WeatherReport(1606953600L, -1.86875, 95, 1023,
                "Celsius", "Tartu", 26.73, 58.38);
        WeatherReport forecast3 = new WeatherReport(1607040000L, 1.10875, 90, 1019,
                "Celsius", "Tartu", 26.73, 58.38);

        List<WeatherReport> forecastList = new LinkedList<>();
        forecastList.add(forecast1);
        forecastList.add(forecast2);
        forecastList.add(forecast3);

        return forecastList;
    }
}
