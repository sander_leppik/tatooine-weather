# Course Project for ICD0004
Building a simple project using Test Driven Developement.

Unit testing and integration testing done with JUnit, Hamcrest and Mockito.

## Project Author:
    Name: Sander Leppik
    Student Code: 185960IADB
    Email: leppiksander@gmail.com

## How to Run

Run all tests: 

``
Terminal> gradle test
``

Tests are located at: src/test/java/


Build JAR: 
```
    Terminal> gradle wrapper
    Terminal> gradle build
    Terminal> gradle jar
```
Jar file at: build/libs/TatooineWeather-1.0.jar

#### How to run:
```
--->cd build/libs/
java -jar TatooineWeather-1.0.jar
```

## Commandline arguments:
```
*   '-simple' - For current weather data
*   '-full' - For current weather data + weather forecast
*   '-city "<City Name>"' - Returns report in standard output
*   '-F "<input file path(.txt)>"' 
*   '-O "<output directory path>"' - 
```
```
Example: '-simple -city Tallinn' - For current weather info in console
Example: '-full -F C:\home\cities\list.txt -O C:\home\weather_reports\ '
```


## Tech Stack:
Project Repo: [Gitlab](https://gitlab.cs.ttu.ee/Sander.Leppik/tatooine-weather)
#### Project
- Java 11
    - Gradle
    - Jackson
    - lombok
    
#### Testing:
- JUnit
- Hamcrest
- Mockito


## TODO

- ~~Weather Engine~~
    - ~~Read api data~~
    - ~~Process weather data~~
- ~~Main~~
    - ~~Read file~~
    - ~~Write file~~
- ~~Use OWM api~~
    - ~~Make an account~~
    - ~~Learn how to use it~~
- Implement all the planets
    - Need to contact NASA