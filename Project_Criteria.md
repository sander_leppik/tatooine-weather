# Project description

Create an application which reads an input file of city names and produces and output file with weather information about those cities:
- Main details:
    - city name
    - Coordinates in the format latitute, longitude: "59.44, 24.75"
    - temperatureUnit
- Current weather report:
    - weather report date
    - temperature in Celsius
    - humidity
    - pressure
- Following 3 day forecast (not counting current date):
    - forecast date
    - average temperature in Celsius
    - average humidity
    - average pressure

Use the [OpenWeatherMap API](https://openweathermap.org/api):
- [current weather data](https://openweathermap.org/current)
- [5 day weather forecast](https://openweathermap.org/forecast5) 


# Project parts

## 1. Main details and current weather

**Note**: 
- Application reads input as string parameter and outputs result to stdout.

### ACCEPTANCE CRITERIA
- City name can be provided as a string input
- The output is a weather report with main details and current weather data
- Main details part has city, coordinates and temperatureUnit properties
- Coordinates are in the format lat, log. Example: "59.44,24.75"
- Current weather part has date, temperature, humidity and pressure properties
- Output is a JSON object
- At least 3 unit tests exists
- Mock integration test exists for OWM for the main details data
- OWM integration is covered by integration tests for the main details data

## 2. Forecast

**Note**:
- Application reads input as string parameter and outputs result to stdout

### ACCEPTANCE CRITERIA
- City name can be provided as a string input
- The output is a weather report with main details and current weather data AND forecast report
- Forecast report part has date, temperature, humidity and pressure for each day
- Forecast calculates average of temperature, humidity and pressure
- Forecast is in ascending order (2020-10-01, 2020-10-02, 2020-10-03)
- At least 3 unit tests exists
- Mock integration test exists for OWM for the forecast data
- OWM integration is covered by integration tests for the forecast data

## 3. Read city name from file and produce a JSON file for given city

**Note**: 
- Application reads input from a file and outputs result to JSON file.

### ACCEPTANCE CRITERIA
- Only specific file format is allowed (you choose which: txt, csv, json, plain, docx)
- Display error message if an unsupported file is provided
- Display error message when file is missing
- Write 3 integration tests to test integrations between the weather report application and file reader and writer

## 4. Read multiple city names from file and produce a JSON output file for each city

**Note**: 
- Application reads input from a file, where there can be multiple city names, and outputs the result for each city to its own JSON file.

### ACCEPTANCE CRITERIA
- Can read multiple cities from file
- Can create weather report for given cities into separate JSON files
- Log INFO message when existing weather report file for city is being overwritten
- When an error occurs for invalid city name(s) then weather reports are created only for valid city names 
- When an error occurs for invalid city name(s) then application logs ERROR message for that city.
